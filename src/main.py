from typing import List
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
from db  import crud, models, schemas
from db.database import SessionLocal, engine


models.Base.metadata.create_all(bind=engine)

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/create-user/", response_model=schemas.User,tags=['user'])
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    ''' Регистрация пользователя '''
    db_user = crud.get_user_by_telegram_chat_id(db, telegram_chat_id=user.telegram_chat_id)
    if db_user:
        raise HTTPException(status_code=400, detail="telegram_chat_id already registered")
    return crud.create_user(db=db, user=user)
